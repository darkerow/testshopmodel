package main.java.com.journaldev.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/*
 * 
 * ����� ��������
 * + ������� ������ �������
 * + ���������� ����
 * 
 */

@WebServlet("/GetShopStateServlet")
public class GetShopStateServlet extends HttpServlet {
	private static final long serialVersionUID = 2L;
	
	Shop shop;
	int steps = 0;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String stCount = request.getParameter("stepsCount");
		
		PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        
        response.setHeader("Cache-control", "no-cache, no-store");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Expires", "-1");
 
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST");
        response.setHeader("Access-Control-Allow-Headers", "Content-Type");
        response.setHeader("Access-Control-Max-Age", "86400");
        
        Gson gson = new Gson();
        JsonObject myObj = new JsonObject();
		
		if(stCount != null && !stCount.isEmpty()) {
			
			String cashdeskCount = request.getParameter("cashdeskCount");
			String manInPercent = request.getParameter("manInPercent");
			String womanInPercent = request.getParameter("womanInPercent");
			String childInPercent = request.getParameter("womanInPercent");
			
			boolean flag = onStart(stCount, cashdeskCount, manInPercent, womanInPercent, childInPercent);

			//������ ������� �������, ����� �������� ������������
	        myObj.addProperty("success", flag);
	        
	        out.println(myObj.toString());
	        out.close();
	        
		} else {
			
			//���
			onStep();
			
			JsonElement shopObj = gson.toJsonTree(shop);
	        myObj.addProperty("steps", steps);
	        myObj.add("ShopInfo", shopObj);
	        
	        out.println(myObj.toString());
	        out.close();
			
		}

	}
	
	private boolean onStart(String stepsCount,
						 String cashdeskCount,
						 String manInPercent,
						 String womanInPercent, 
						 String childInPercent) {
		
		//������� �������
		
		try {
			
			shop = new Shop(0, 
					Integer.parseInt(cashdeskCount),
					Integer.parseInt(manInPercent), 
					Integer.parseInt(womanInPercent), 
					Integer.parseInt(childInPercent));
			
		} catch(NumberFormatException e) {
			return false;
		}
		
		steps = 0;
		
		return true;
		
	}
	
	private void onStep() {
		shop.step();
        steps++;
	}

}

/*
	ie 11
	chrome ������ 41.0.2272.89 m
	firefox 36.0.1
	
*/