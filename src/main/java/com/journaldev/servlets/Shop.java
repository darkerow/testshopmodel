package main.java.com.journaldev.servlets;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/*
 * 
 * ������ ��������
 * + ������������� ����
 * + ����� ����������
 * 
 */

public class Shop {
	
	List<Cashdesk> cashdesks = null;
	BuyerFactory factory = null;
	
	public Shop(int steps) {
		
		cashdesks = new ArrayList<Cashdesk>();
	
		cashdesks.add(new Cashdesk(1));
		cashdesks.add(new Cashdesk(2));

		factory = new BuyerFactory();
		
	}
	
	public Shop(int steps, 
			 	int cashdeskCount,
			 	int manInPercent,
			 	int womanInPercent, 
			 	int childInPercent) {
		
		cashdesks = new ArrayList<Cashdesk>();
		
		//������� �����
		for(int i = 0; i < cashdeskCount; i++) {
			Cashdesk cashdesk = Cashdesk.createWithRandomProductivity();
			cashdesks.add(cashdesk);			
		}
		
		factory = new BuyerFactory(manInPercent, womanInPercent, childInPercent);
	}
	
	public void step() {
		
		//��������� ���
		for(Cashdesk cashdesk : cashdesks) {
			cashdesk.step();
		}
		
		//������� ������ ����������
		generateNewBuyer();
		
	}
	
	public void generateNewBuyer() {
		//������� ������ buyer
		Buyer buyer = factory.createRandom();
		int whatQueue = buyer.solveWhatQueue(cashdesks);
		cashdesks.get(whatQueue).addBuyer(buyer);
	}
	
}
