package main.java.com.journaldev.servlets;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Random;

/*
 * 
 * ����� �����
 * + ����������� �����������
 * 
 */

public class Cashdesk {
	
	/*********************************************/
	static private final int productivityMax = 6;
	/*********************************************/
	
	//������������������ �����
	private int productivity;
	//������ �����������
	private List<Buyer> buyers = null;
	
	public Cashdesk(int productivity) {
		this.productivity = productivity;
		buyers = new ArrayList<Buyer>();
	}
	
	//��������� ����� ��� �������� ����� � ��������� �������������������
	static public Cashdesk createWithRandomProductivity() {
		Random rand = new Random();
		int randomProductivity = Math.abs(rand.nextInt() % productivityMax) + 1;
		return new Cashdesk(randomProductivity);
	}
	
	public void addBuyer(Buyer buyer) {
		buyers.add(buyer);
	}
	
	public void step() {
		//���� ���������� ���� � �������
		if(!buyers.isEmpty()) {
			
			//�������� ���������� ������� �� ���
			Buyer buyerFirst = buyers.get(0);
			buyerFirst.step();
			buyerFirst.removeItem(productivity);
			
			//���� ����� ���������, ��������� � ���� ����������
			if(!buyerFirst.hasItems()) {
				buyers.remove(buyerFirst);
			}
			
			if(buyers.size() >= 1) {
				//�������, ���������� � ������� - �� �������
				Buyer buyerLast = buyers.get(buyers.size() - 1);
				buyerLast.step();
			}
		}
		
	}
	
	public int getProductivity() {
		return productivity;
	}
	
	public int getQueueSize() {
		return buyers.size();
	}
	
	//���������� ���������� ����� ��� ������������� ���� �������
	public int getStepsToProcessQueue() {
		
		int steps = 0;
		
		for(Buyer buyer : buyers) {
			
			int itemsCnt = buyer.getItemsCnt();
			
			int div = (int)Math.ceil((double)itemsCnt / productivity);
			steps += div;
			/*
			System.out.println("itemsCnt: " + String.valueOf(itemsCnt) +
					" productivity: " + String.valueOf(productivity) +
					" result /:" + String.valueOf(div));// +
			*/
		}
		
		return steps;
		
	}
}
