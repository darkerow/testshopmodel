package main.java.com.journaldev.servlets;

import java.util.Random;

public class BuyerFactory {
	
	/*********************************************/
	static private final int buyerItemsMaxCount = 8;
	static private final int buyerTypesCount = 3;
	/*********************************************/
	
	private int manChance;
	private int womanChance;
	private int childChance;
	
	public BuyerFactory() {
		this.manChance = 33;
		this.womanChance = 33;
		this.childChance = 33;
	}
	
	public BuyerFactory(int manChance, int womanChance, int childChance) {
		this.manChance = manChance;
		this.womanChance = womanChance;
		this.childChance = childChance;
	}
	
	public void setChances(int manChance, int womanChance, int childChance) {
		this.manChance = manChance;
		this.womanChance = womanChance;
		this.childChance = childChance;
	}
	
	public Buyer createRandom() {
		Random rand = new Random();
		int randomBuyer = getRandomType();//Math.abs(rand.nextInt() % 3);
		int randomItemsCount = Math.abs(rand.nextInt() % this.buyerItemsMaxCount) + 1;
		
		Buyer newBuyer = null;
		
		switch(randomBuyer) {
			case 0: newBuyer = new ManBuyer(randomItemsCount); break;
			case 1: newBuyer = new WomanBuyer(randomItemsCount); break;
			case 2: newBuyer = new ChildBuyer(randomItemsCount); break;
		}
		
		return newBuyer;
	}
	
	
	//���������� ��� ���������� � ����������� �� ��� ����� ���������
	private int getRandomType() {
		
		Random rand = new Random();
		int ranType = 0;
		
        while (true)
        {
            ranType = Math.abs(rand.nextInt() % this.buyerTypesCount);
            int chanceLimit = getChanceForType(ranType);
            int ranChance = Math.abs(rand.nextInt() % 99) + 1;
        
            //Debug.Log(string.Format("ran chance: {0} and chance limit: {1}", ranChance, chanceLimit));
        
            if(ranChance <= chanceLimit)
            {
                //Debug.Log("selected type: " + ranType.ToString()); 
                break;//�����!
            }
        }
        
        return ranType;
		
	}
	
	private int getChanceForType(int type) {
		switch(type) {
			case 0: return manChance;
			case 1: return womanChance;
			case 2: return childChance;
		}
		
		return 0;
	}
	
}
