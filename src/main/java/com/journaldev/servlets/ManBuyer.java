package main.java.com.journaldev.servlets;

import java.util.List;

public class ManBuyer extends Buyer {

	public ManBuyer(int itemsCount) {
		super(itemsCount);
		tag = "Man";
	}

	@Override
	public int solveWhatQueue(List<Cashdesk> cashdesks) {
		
		//�������� �����, � ������� ��� ��������� �� ���������� ����� �����
		int minSteps = Integer.MAX_VALUE;
		int selectedCashdesk = 0;
		
		//System.out.println(">>>>>>>>>man solver");
		for(int i = 0; i < cashdesks.size(); i++) {
			Cashdesk cashdesk = cashdesks.get(i);
			
			int currentCashdeskSteps = cashdesk.getStepsToProcessQueue();
			int curBuyerStepsForCurCashdesk = (int)Math.ceil((double)this.itemsCount / cashdesk.getProductivity());
			int curSumSteps = currentCashdeskSteps + curBuyerStepsForCurCashdesk;
			
			if(curSumSteps < minSteps) {
				minSteps = curSumSteps;
				selectedCashdesk = i;
			}
		}
		
		//System.out.println(">>>>>>>>>man go to: " + String.valueOf(selectedCashdesk)
		//		+ ", by minSteps: " + String.valueOf(minSteps));
		
		return selectedCashdesk;
	}

}
