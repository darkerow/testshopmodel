package main.java.com.journaldev.servlets;

import java.util.List;
import java.util.Random;

public class ChildBuyer extends Buyer {

	public ChildBuyer(int itemsCount) {
		super(itemsCount);
		tag = "Child";
	}

	@Override
	public int solveWhatQueue(List<Cashdesk> cashdesks) {
		//�������� ������� ��������
		Random rand = new Random();
		int randomCashdesk = Math.abs(rand.nextInt() % cashdesks.size());

		return randomCashdesk;
	}

}
