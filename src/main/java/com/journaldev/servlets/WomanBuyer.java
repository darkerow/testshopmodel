package main.java.com.journaldev.servlets;

import java.util.List;

public class WomanBuyer extends Buyer {

	public WomanBuyer(int itemsCount) {
		super(itemsCount);
		tag = "Woman";
	}

	@Override
	public int solveWhatQueue(List<Cashdesk> cashdesks) {
		
		int minQueue = Integer.MAX_VALUE;
		int selectedCashdesk = 0;
		
		//наименьшая очередь
		for(int i = 0; i < cashdesks.size(); i++) {
			Cashdesk cashdesk = cashdesks.get(i);
			
			if(cashdesk.getQueueSize() < minQueue) {
				minQueue = cashdesk.getQueueSize();
				selectedCashdesk = i;
			}
		}
		
		return selectedCashdesk;
	}

}
