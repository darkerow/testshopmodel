package main.java.com.journaldev.servlets;

import java.util.List;

/*
 * ����������� ����� ����������
 */

public abstract class Buyer {
	
	//������ �� ���� ����
	protected boolean isNew;
	//tag ��� �������� � json ������� (� apps.js) 
	public String tag;
	protected int itemsCount;
	
	public Buyer(int itemsCount) {
		this.itemsCount = itemsCount;
		this.isNew = true;
	}
	
	public void step() {
		if(isNew == true)
			isNew = false;
	}
	
	public void removeItem(int countToRemove) {
		itemsCount -= countToRemove;
	}
	
	public boolean hasItems() {
		return itemsCount > 0;
	}
	
	public int getItemsCnt() {
		return itemsCount;
	}
	
	//�����, ������� ������ � ����� ����� ���������� �������
	public abstract int solveWhatQueue(List<Cashdesk> cashdesks);
	
}
