<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>My Mega Shop</title>
 
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/apps.js"></script>
 
</head>
<body>
 
    <form id="myAjaxRequestForm">
        <fieldset>
            <legend>My Mega Shop</legend>
 				<p>
                    <label for="cashdeskCount">Cashdesk count:</label>
                    <br>
                    <input id="cashdeskCount" type="text" name="cashdeskCount" value="2"/>
                </p>
                <p>
                    <label for="stepsCount">Steps count:</label>
                    <br>
                    <input id="stepsCount" type="text" name="stepsCount" value="10"/>
                </p>
                <p>
                    <label for="manPercent">Man chance:</label>
                    <br>
                    <input id="manPercent" type="range" min="5" max="100" name="manPercent" value="30"/>
                    <input type="text" name="manChance" id="manChance" class="text" value="30" size = 1>
                </p>
                <p>
                    <label for="womanPercent">Woman chance:</label>
                    <br>
                    <input id="womanPercent" type="range" min="5" max="100" name="womanPercent"  value="30"/>
                    <input type="text" name="womanChance" id="womanChance" class="text" value="30" size = 1>
                </p>
                <p>
                    <label for="childPercent">Child chance:</label>
                    <br>
                    <input id="childPercent" type="range" min="5" max="100" name="childPercent" value="30"/>
                    <input type="text" name="childChance" id="childChance" class="text" value="30" size = 1>
                </p>
                <p>
                    <input id="myButton" type="button" value="Submit" />
                </p>
        </fieldset>
    </form>
    <div id="anotherSection">
        <fieldset>
            <legend>Response from ShopServlet</legend>
                 <div id="ajaxResponse"></div>
        </fieldset>
    </div>   
 
</body>
</html>