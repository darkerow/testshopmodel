$(document).ready(function() {
 
	$.ajaxSetup ({cache: false});
	
	checkSliders();
	
    //Stops the submit request
    $("#myAjaxRequestForm").submit(function(e){
           e.preventDefault();
    });
    
    //checks for the button click event
    $("#myButton").click(function(e){
    	
            //get the form data and then serialize that
            //dataString = $("#myAjaxRequestForm").serialize();
            
            //get the form data using another method 
            var stepsCount = $("input#stepsCount").val(); 
            var cashdeskCount = $("input#cashdeskCount").val();
            var manPercent = $("input#manPercent").val(); 
            var womanPercent = $("input#womanPercent").val(); 
            var childPercent = $("input#childPercent").val();
            
            dataString = "stepsCount=" + stepsCount;
            dataString += "&cashdeskCount=" + cashdeskCount;
            dataString += "&manInPercent=" + manPercent;
            dataString += "&womanInPercent=" + womanPercent;
            dataString += "&childInPercent=" + cashdeskCount;
            
            //make the AJAX request, dataType is set to json
            //meaning we are expecting JSON data in response from the server
            
            $.ajax({
                type: "GET",
                url: "GetShopStateServlet",
                data: dataString,
                dataType: "json",
                async : false,
                
                //if received a response from the server
                success: function( data, textStatus, jqXHR) {
                	
                     if(data.success){
                    	 $("#ajaxResponse").html("");
                    	 //запускаем пошаговый
                    	 getStepsAjax(stepsCount);
                     } 
                     //display error message
                     else {
                         $("#ajaxResponse").html("<div><b>Invalid input!</b></div>");
                     }
                },
                
                //If there was no resonse from the server
                error: function(jqXHR, textStatus, errorThrown){
                     console.log("Something really bad happened " + textStatus);
                      $("#ajaxResponse").html(jqXHR.responseText);
                },
            }); 
            
               
    });
 
});



function getStepsAjax( steps ) {
	
	for (k = 0; k < steps; ++k) {
        
        $.ajax({
            type: "GET",
            url: "GetShopStateServlet",
            dataType: "json",
            async : false,
            
            //if received a response from the server
            success: function( data, textStatus, jqXHR) {
                     //$("#ajaxResponse").html("");
            	
            		 var ajRes = $("#ajaxResponse");
            	
            		 ajRes.append("<br>----------------------------------------------");
            		 ajRes.append("<br>" + "<b>Step:</b> " + data.steps);
                    
                     //image size
                     var im_size = 40;
                     var im_size_delta = im_size - 10;
                     
                     
                     //визуализируем модель магазина на текущем шаге
                     ajRes.append("<table>");
            		 var cashdesks = data.ShopInfo.cashdesks;
                     for (i = 0; i < cashdesks.length; ++i) {
                    	 ajRes.append('<br><br>');
                    	 ajRes.append('<tr>');
                    	 
                    	 var image = $('<img></img>');
                    	 image.attr('src', 'imgs/cashdesk_100.jpg');
                		 image.width(im_size).height(im_size);
                		 ajRes.append(image);
                		 ajRes.append(cashdesks[i].productivity);
            			 
                    	 var buyers = cashdesks[i].buyers;
                    	 for (j = 0; j < buyers.length; ++j) {

                    		 var image = $('<img></img>');
                    		 if(buyers[j].tag == "Man")
                    			 image.attr('src', 'imgs/man_100.jpg');
                    		 else if(buyers[j].tag == "Woman")
                    			 image.attr('src', 'imgs/woman_100.jpg');
                    		 else 
                    			 image.attr('src', 'imgs/child_100.jpg');
                    			 
                    		 image.width(im_size).height(im_size);
                    		 
                    		 ajRes.append(image);
                    		 ajRes.append(buyers[j].itemsCount);
                			 
                			 if(buyers[j].isNew) {
                				 image.each(function setAnim(){
                     			    $(this).
                     			            animate({opacity : 0.1},200).
                     			            animate({opacity : 1.0},200,setAnim);
                     			});
                			 }
                    	 }
                    	 ajRes.append('</tr>');
                     }
                     ajRes.append("</table>");
                     
            },
            
            error: function(jqXHR, textStatus, errorThrown){
                 console.log("Error:  " + textStatus);
                  $("#ajaxResponse").html(jqXHR.responseText);
            },
  
        });   
	} 
}

function checkSliders() {
	
	$('#manPercent').on('change', function(){
	    $('#manChance').val($(this).val());
	});
	$('#manPercent').on('mousemove', function(){
	    $('#manChance').val($(this).val());
	});
	
	$('#womanPercent').on('change', function(){
	    $('#womanChance').val($(this).val());
	});
	$('#womanPercent').on('mousemove', function(){
	    $('#womanChance').val($(this).val());
	});
	
	$('#childPercent').on('change', function(){
	    $('#childChance').val($(this).val());
	});
	$('#childPercent').on('mousemove', function(){
	    $('#childChance').val($(this).val());
	});
	
}
